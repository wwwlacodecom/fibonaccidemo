import React from 'react';
import { StyleSheet, Text, View, FlatList, Button } from 'react-native';

export default class App extends React.Component {

state = {
  users2: [],
}


 Fibonacci(num){
  var a = 1, b = 0, temp;

  while (num >= 0 && a<9007199254740991){
    temp = a;
    a = a + b;

   // console.log(a)
let floors = [...this.state.users2];

this.state.users2.push({ key: a.toString() });

this.setState({users2: floors });
    b = temp;
    num--;
  }


  return b;
  
}
runFibonacci= () =>  {
  this.Fibonacci(30000)
}


  render() {

    return (
      <View style={styles.container} >
      <Text style={styles.h2text}>
        Fibonacci 
      </Text>

      <FlatList
  data={this.state.users2}
  renderItem={({item}) => <Text>{item.key}</Text>}
/>


    <Button title="Generate Fibonacci Sequence" onPress={this.runFibonacci} />

  
    </View>
    );

  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 50,
    marginBottom: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  h2text: {
    marginTop: 10,
    fontFamily: 'Helvetica',
    fontSize: 36,
    fontWeight: 'bold',
  },
  flatview: {
    justifyContent: 'center',
    paddingTop: 30,
    borderRadius: 2,
  },
  name: {
    fontFamily: 'Verdana',
    fontSize: 18
  },
  email: {
    color: 'red'
  }
});
